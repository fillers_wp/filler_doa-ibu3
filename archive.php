<?php get_header(); ?>
<div id="container">
<div id="contents">

<div id="contents-right">
<div class="featured" > 
<?php fastestwp_breadcrumbs(); ?>
<h1><?php if(is_category()) { ?> <?php single_cat_title(''); ?>
    <?php } elseif (is_day()) { ?><?php the_time('F jS, Y'); ?>
	<?php } elseif (is_month()) { ?> <?php the_time('F, Y'); ?>
	<?php } elseif (is_tag()) { ?> <?php single_tag_title(''); ?>
	<?php } elseif (is_year()) { ?> <?php the_time('Y'); ?>
	<?php } elseif (is_author()) { ?> Author
	<?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?> Blog Archives
	<?php }  if ( get_query_var('paged') ) { echo ' ('; echo __('page') . ' ' . get_query_var('paged');   echo ')';  } ?></h1>
<?php $top1_act = of_get_option('top1_act'); if(($top1_act == '1')) { ?><div class="ads-content"><?php echo of_get_option('ads1_top'); ?></div><?php } ?>
</div>
<?php get_template_part( 'loop' ); ?>
<div class="featured" > 
<?php $bottom1_act = of_get_option('bottom1_act'); if(($bottom1_act == '1')) { ?><div class="ads-content"><?php echo of_get_option('ads1_bottom'); ?></div>
<?php } ?>
</div>

<?php fastestwp_pagenavi(); ?>	
</div>
<div id="contents-left">
<?php get_template_part( 'sidebar-left' ); ?>
</div>


</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>