<?php
if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_bloginfo('template_directory') . '/functions/admin/' );
	require_once dirname( __FILE__ ) . '/functions/admin/options-framework.php';
}
define( 'FASTESTWP_BASE_DIR', TEMPLATEPATH . '/' );
define( 'FASTESTWP_BASE_URL', get_template_directory_uri() . '/' );
if( !isset( $content_width ) )
	$content_width = 530;
if( !function_exists('fastestwp_theme_setup') ) {
	function fastestwp_theme_setup() {
		add_theme_support('post-thumbnails');
        register_nav_menus(         array(
		'main-menu' => __('Main Navigation')
        ) );
	}
}
add_action( 'after_setup_theme', 'fastestwp_theme_setup' );
include( FASTESTWP_BASE_DIR . 'functions/options.php' );
include( FASTESTWP_BASE_DIR . 'functions/add-option.php' );
include( FASTESTWP_BASE_DIR . 'functions/widget.php' );
include( FASTESTWP_BASE_DIR . 'functions/function.php' );
include( FASTESTWP_BASE_DIR . 'functions/script/doaibu.php' );
include( FASTESTWP_BASE_DIR . 'functions/script/aq_resizer.php' );
?>