<?php
add_filter( 'the_category', 'add_nofollow_cat' ); 
function add_nofollow_cat( $text ) {
$text = str_replace('rel="category tag"', "", $text); return $text;
}
?>
<?php
if ( !function_exists('fastestwp_custom_styles') ) {
	function fastestwp_custom_styles() {
          $color1 = of_get_option('fastestwp_color');
		  $color2 = of_get_option('link_color');
		  $background = of_get_option('fastestwp_background_color');
          		?>

<style type="text/css">
body{
	margin:0px auto 0px;
	padding:0px;
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	color:#000;
	line-height: 18px;
<?php if ($background) {
if ($background['image']) {
echo 'background:url('.$background['image']. ') ;';
echo 'background-repeat:'.$background['repeat']. ' ;';
echo 'background-color:'.$background['color']. ' ;';
echo 'background-position:'.$background['position']. ' ;';
echo 'background-attachment:'.$background['attachment']. ' ;';
} else {
echo 'background-color:'.$background['color'].';';
}	
; 	
}; ?>
}
a{color:<?php echo $color1; ?>;}
h1{	color:<?php echo $color1; ?>;}
h2{color:<?php echo $color1; ?>;}
h3{color:<?php echo $color1; ?>;}
h4{color:<?php echo $color1; ?>;}
#nav span:hover{background-color:<?php echo $color1; ?>;}
.catmenu a:hover {background-color:<?php echo $color1; ?>;}
.dropdown .current-menu-item a{color:<?php echo $color1; ?>;}
.current{color:<?php echo $color1; ?>;}
.dropdown a:hover{background-color:<?php echo $color1; ?>;}
.dropdown li * a:hover{background-color:<?php echo $color1; ?>;}
</style>
<?php
}
}
add_action('wp_head', 'fastestwp_custom_styles');
?>
<?php
function fastestwp_enqueue_scripts() {
    wp_enqueue_script( 'jquery' );
}    
add_action('wp_enqueue_scripts', 'fastestwp_enqueue_scripts');
?>
<?php 
remove_action('wp_head', 'wp_generator');
function _remove_script_version( $src ){
    $parts = explode( '?', $src );
    return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );
?>