<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {
	

	// Background Defaults
	$background_defaults = array(
		'color' => '#ebebeb',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );
		
	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();
	// tab-1

	$options[] = array( "name" => "Styling",
						"type" => "heading");
	
	$options[] = array( "name" => "Header",
						"desc" => "upload your header size 300 x 70 px.",
						"id" => "logo_uploader",
						"type" => "upload");
	$wp_editor_settings = array(
		'wpautop' => true, 
		'textarea_rows' => 5,
		'tinymce' => false,
		'quicktags'=> false
	);						
	$options[] = array( "name" => "Favicon",
						"desc" => "Upload your favicon",
						"id" => "favicon_uploader",
						"type" => "upload");

$options[] = array( "name" => "Background Color",
						"desc" => "Choose Color background",
						"id" => "fastestwp_background_color",
						"std" => $background_defaults, 
						"type" => "background");			
						
	$options[] = array( "name" => "Theme Color",
						"desc" => "Choose link Color",
						"id" => "fastestwp_color",
						"std" => "#ff7700",
						"type" => "color");
															
						  																				
	$options[] = array( "name" => "Footer",
						"desc" => "footer text , you can insert stat code right here",
						"id" => "footer_text",
						'type' => 'editor',
						'settings' => $wp_editor_settings );
						
	$options[] = array( "name" => "Facebook App ID",
						"desc" => "create FB app ID >> https://developers.facebook.com/apps/",
						"id" => "fb_id",
						"std" => "",
						"type" => "text"); 

    $options[] = array( "name" => "use timthumb?",
						"desc" => "Please select",
						"id" => "thumb_act",
						"std" => "No",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array("No", "Yes"));
																															
	// ads setting	
				
	$options[] = array( "name" => "Adsense",
						"type" => "heading");
	
		$options[] = array( "name" => "display ads at Header ?",
						"desc" => "Please select",
						"id" => "header1_act",
						"std" => "No",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array("No", "Yes"));
												
		$options[] = array( "name" => "Ads 728 x 90",
						"desc" => "input your ads code size 728 x 90",
						"id" => "ads1_728",
						'type' => 'editor',
						'settings' => $wp_editor_settings );
						
						
					
	$options[] = array( "name" => "display ads at top?",
						"desc" => "Please select",
						"id" => "top1_act",
						"std" => "No",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array("No", "Yes"));
						
												
		$options[] = array( "name" => "Ads Top Code",
						"desc" => "you can input your ads code size 336x280 or 300x250 or 468x60",
						"id" => "ads1_top",
						'type' => 'editor',
						'settings' => $wp_editor_settings ); 
						
	$options[] = array( "name" => "display ads at bottom?",
						"desc" => "Please select",
						"id" => "bottom1_act",
						"std" => "No",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array("No", "Yes"));
						
		$options[] = array( "name" => "Ads Bottom Code",
						"desc" => "you can input your ads code size 336x280 or 300x250 or 468x60",
						"id" => "ads1_bottom",
						'type' => 'editor',
						'settings' => $wp_editor_settings );
						  
		$options[] = array( "name" => "Ads 300 x 250 sidebar",
						"desc" => "to display this ads you must drag and drop widget wp-admin >> appearance >> widget>> virtarich ads sidebar 300X250",
						"id" => "ads1_300",
						'type' => 'editor',
						'settings' => $wp_editor_settings );	
											
		$options[] = array( "name" => "Ads 160 x 600 sidebar",
						"desc" => "to display this ads you must drag and drop widget wp-admin >> appearance >> widget>> virtarich ads sidebar 160X600",
						"id" => "ads1_160",
						'type' => 'editor',
						'settings' => $wp_editor_settings );
						
	// ads non adsense						
						
	$options[] = array( "name" => "Non Adsense",
						"type" => "heading");
						
		$options[] = array( "name" => "display ads at Header ?",
						"desc" => "Please select",
						"id" => "header2_act",
						"std" => "No",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array("No", "Yes"));
												
		$options[] = array( "name" => "Ads 728 x 90",
						"desc" => "input your ads code size 728 x 90",
						"id" => "ads2_728",
						'type' => 'editor',
						'settings' => $wp_editor_settings );

						
					
	$options[] = array( "name" => "display ads at top?",
						"desc" => "Please select",
						"id" => "top2_act",
						"std" => "No",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array("No", "Yes"));
						
												
		$options[] = array( "name" => "Ads Top Code",
						"desc" => "you can input your ads code size 336x280 or 300x250 or 468x60",
						"id" => "ads2_top",
						'type' => 'editor',
						'settings' => $wp_editor_settings ); 
						
	$options[] = array( "name" => "display ads at bottom?",
						"desc" => "Please select",
						"id" => "bottom2_act",
						"std" => "No",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => array("No", "Yes"));
						
		$options[] = array( "name" => "Ads Bottom Code",
						"desc" => "you can input your ads code size 336x280 or 300x250 or 468x60",
						"id" => "ads2_bottom",
						'type' => 'editor',
						'settings' => $wp_editor_settings );
						  
		$options[] = array( "name" => "Ads 300 x 250 sidebar",
						"desc" => "to display this ads you must drag and drop widget wp-admin >> appearance >> widget>> virtarich ads sidebar 300X250",
						"id" => "ads2_300",
						'type' => 'editor',
						'settings' => $wp_editor_settings );						

		$options[] = array( "name" => "Ads 160 x 600 sidebar",
						"desc" => "to display this ads you must drag and drop widget wp-admin >> appearance >> widget>> virtarich ads sidebar",
						"id" => "ads2_160",
						'type' => 'editor',
						'settings' => $wp_editor_settings );
						
												
																							
	return $options;
}