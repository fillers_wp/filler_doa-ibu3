<?php 
function fastestwp_sidebars() {
register_sidebar(array('name'=>'left Sidebar',
'before_widget' => '<div class="box">', 
'after_widget' => '</div>', 
'before_title' => '<h4>', 
'after_title' => '</h4>', 
));
register_sidebar(array('name'=>'right Sidebar',
'before_widget' => '<div class="box">', 
'after_widget' => '</div>', 
'before_title' => '<h4>', 
'after_title' => '</h4>', 
));
}

add_action( 'widgets_init', 'fastestwp_sidebars' );
?>
<?php
function virtarich_latest_post() { ?>
<div class="box">
<h4>Recent Post</h4>
<?php global $post; query_posts('posts_per_page=4');
if (have_posts()) :  while (have_posts()) : the_post(); ?>
<?php get_template_part( 'small-thumb' ); ?>
<?php endwhile; endif; ?>
<?php wp_reset_query(); ?>
<div style="clear: both"></div> 	
</div>
<?php
}
wp_register_sidebar_widget(
    'virtarich_latest_post_1', 
    'virtarich latest post', 
    'virtarich_latest_post',
    array( 
        'description' => 'Latest Post With Thumb Widget'
    )
);
?>
<?php
function virtarich_random_post() { ?>
<div class="box">
<h4>Random Post</h4>
<?php 
$my_query = new WP_Query('posts_per_page=4&orderby=rand');
while ($my_query->have_posts()) : $my_query->the_post(); 
?>		
<?php get_template_part( 'small-thumb'); ?>
<?php endwhile; wp_reset_postdata(); ?>
</div>
<?php
}
wp_register_sidebar_widget(
    'virtarich_random_post_2',
    'virtarich random post',
    'virtarich_random_post',
    array( 
        'description' => 'Random Post With Thumb Widget'
    )
);
?>
<?php
function virtarich_ads_sidebar2() { ?>
<div class="box">

<?php if ( is_home() || is_category() ) { ?>

<?php echo of_get_option('ads1_300'); ?>

<?php } elseif ( is_attachment() ) { ?>

<?php echo of_get_option('ads2_300'); ?>

<?php } elseif ( is_single() ) { ?>

<?php echo of_get_option('ads1_300'); ?>

<?php } else { ?>

<?php echo of_get_option('ads2_300'); ?>

<?php } ?>

</div>
<?php
}
wp_register_sidebar_widget(
    'virtarich_ads_sidebar2',
    'virtarich ads sidebar 300X250',
    'virtarich_ads_sidebar2',
    array( 
        'description' => 'insert code from theme option'
    )
);
?>
<?php
function virtarich_ads_sidebar() { ?>
<div class="box">

<?php if ( is_home() || is_category() ) { ?>

<?php echo of_get_option('ads1_160'); ?>

<?php } elseif ( is_attachment() ) { ?>

<?php echo of_get_option('ads2_160'); ?>

<?php } elseif ( is_single() ) { ?>

<?php echo of_get_option('ads1_160'); ?>

<?php } else { ?>

<?php echo of_get_option('ads2_160'); ?>

<?php } ?>

</div>
<?php
}
wp_register_sidebar_widget(
    'virtarich_ads_sidebar',
    'virtarich ads sidebar 160X600',
    'virtarich_ads_sidebar',
    array( 
        'description' => 'insert code from theme option'
    )
);
?>