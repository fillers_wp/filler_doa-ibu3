<!DOCTYPE html>
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<head>
<meta charset="utf-8">
<title> <?php if ( is_home() ) { ?><?php bloginfo('name'); ?> - <?php bloginfo('description'); } else 
{ ?><?php  wp_title(''); ?> - <?php bloginfo('name'); } ?></title>
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<?php echo get_template_directory_uri(); ?>/media.css" rel="stylesheet" type="text/css">
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( of_get_option('favicon_uploader') ) { ?>
<link rel="Shortcut Icon" href="<?php echo of_get_option('favicon_uploader'); ?>" type="image/x-icon" />
<?php } else {?>
<link rel="Shortcut Icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico" type="image/x-icon" />
<?php } ?>
<?php wp_head(); ?>
<meta property='fb:app_id' content='<?php echo of_get_option('fb_id'); ?>' /> 
<meta property="og:image" content="<?php echo fastestwp_first_image() ?>"/>
</head>
<body <?php body_class(''); ?>> 
<div id="wrap">
<header id="header">
<section id="header-left">
<?php if( of_get_option('logo_uploader') ){ ?><a href="<?php echo home_url() ; ?>" title="<?php bloginfo('name'); ?>">
<img src="<?php echo of_get_option('logo_uploader'); ?>" alt="<?php bloginfo('name'); ?>" width="300" height="90"></a>
<?php } else { ?>
<div class="logo"><a href="<?php echo home_url() ; ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></div>
<div class="desc"><?php bloginfo('description'); ?></div>
<?php } ?>
</section>
<section id="header-right">
<form method="get" id="search" action="<?php bloginfo('url'); ?>/">
<input id="search-box" type="text" value="Search ... " onfocus="if
(this.value==this.defaultValue) this.value='';" name="s" size="20" />
<input id="search-button" type="submit" value="Search" />
</form>
</section>
</header>
<div style="clear: both"></div>
<nav id="nav"><a href="<?php echo home_url(); ?>"><span></span></a>
<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_class' => 'dropdown' , 'fallback_cb' => '' ) ); ?>
<div style="clear: both"></div></nav>

<?php if ( is_home() || is_category() ) { ?>

<?php $header1_act = of_get_option('header1_act'); if(($header1_act == '1')) { ?>

<div id="topads-left"><?php echo of_get_option('ads1_728'); ?></div>

<?php } ?>

<?php } elseif ( is_attachment() ) { ?>

<?php $header2_act = of_get_option('header2_act'); if(($header2_act == '1')) { ?>
<div id="topads-left"><?php echo of_get_option('ads2_728'); ?></div>
<?php } ?>

<?php } elseif ( is_single() ) { ?>

<?php $header1_act = of_get_option('header1_act'); if(($header1_act == '1')) { ?>
<div id="topads-left"><?php echo of_get_option('ads1_728'); ?></div>
<?php } ?>

<?php } else { ?>

<?php $header2_act = of_get_option('header2_act'); if(($header2_act == '1')) { ?>
<div id="topads-left"><?php echo of_get_option('ads2_728'); ?></div>
<?php } ?>

<?php } ?>
