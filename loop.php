<?php if (have_posts()) : while (have_posts()) : the_post();  ?>
<article class="post" > 
<?php get_template_part( 'thumb' ); ?>
<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
 
<?php echo fastestwp_excerpt(32); ?> 

<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">... read more</a>

</article>
<?php endwhile; ?>
<?php else : ?>
<article class="post"><h2>Not Found</h2>Sorry, but you are looking for something that isn't here.</article>
<?php endif; ?>