<?php get_header(); ?>
<div id="container">
<div id="contents">
<div id="contents-right">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<article class="post">
<?php fastestwp_breadcrumbs(); ?>
<h1><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>

<?php $top2_act = of_get_option('top2_act'); if(($top2_act == '1')) { ?><div class="ads-content"><?php echo of_get_option('ads2_top'); ?></div><?php } ?>

<?php the_content(); ?>

<?php $bottom2_act = of_get_option('bottom1_act'); if(($bottom2_act == '1')) { ?><div class="ads-content"><?php echo of_get_option('ads2_bottom'); ?></div>
<?php } ?>

</article>
<?php endwhile; ?><?php else : ?>
<article class="post"><h2>Not Found</h2>Sorry, but you are looking for something that isn't here.</article>
<?php endif; ?>
</div>
<div id="contents-left">
<?php get_template_part( 'sidebar-left' ); ?>
</div>

</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>