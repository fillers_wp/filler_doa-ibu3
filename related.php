<h2> Related For <?php the_title(); ?></h2>
<?php 
global $post; 
$categories = get_the_category($post->ID);
if ($categories) { $category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array('category__in' => $category_ids,'post__not_in' => array($post->ID),'posts_per_page'=> 4, 'orderby' => 'rand' );
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {while( $my_query->have_posts() ) { $my_query->the_post();?>
<?php get_template_part( 'small-thumb'); ?>
<?php
}
}
}
wp_reset_query(); ?>