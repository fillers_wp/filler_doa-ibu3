<?php get_header(); ?>
<div id="container">
<div id="contents">

<div id="contents-right">
<div class="featured" > 
<?php fastestwp_breadcrumbs(); ?>
<h1><?php the_search_query();?> </h1>

<?php $top2_act = of_get_option('top2_act'); if(($top2_act == '1')) { ?><div class="ads-content"><?php echo of_get_option('ads2_top'); ?></div><?php } ?>
</div>
<?php get_template_part( 'loop' ); ?>

<?php $bottom2_act = of_get_option('bottom1_act'); if(($bottom2_act == '1')) { ?><div class="ads-content"><?php echo of_get_option('ads2_bottom'); ?></div>
<?php } ?>

<?php fastestwp_pagenavi(); ?>

</div>
<div id="contents-left">
<?php get_template_part( 'sidebar-left' ); ?>
</div>


</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>