<?php get_header(); ?>
<div id="container">
<div id="contents">

<div id="contents-right">

<?php if (have_posts()) : while (have_posts()) : the_post();  ?>
<article class="post">
<?php fastestwp_breadcrumbs(); ?>
<h1><?php the_title(); ?></h1>


<?php $top1_act = of_get_option('top1_act'); if(($top1_act == '1')) { ?><div class="ads-content"><?php echo of_get_option('ads1_top'); ?></div><?php } ?>

<?php the_content(); ?>	

<?php $bottom1_act = of_get_option('bottom1_act'); if(($bottom1_act == '1')) { ?><div class="ads-content"><?php echo of_get_option('ads1_bottom'); ?></div><?php } ?>

<div style="clear: both"></div>


<section class="tags"><?php the_tags('tags: ',', ',''); ?></section>


<section class="related"><?php get_template_part( 'related' ); ?></section>


</article>
<?php endwhile; ?>

<?php else : ?>
<article class="post"><h2>Not Found</h2>Sorry, but you are looking for something that isn't here.</article>
<?php endif; ?>

</div>
<div id="contents-left">
<?php get_template_part( 'sidebar-left' ); ?>
</div>


</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>